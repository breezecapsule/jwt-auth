package user.auth.plugin

import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jws
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureException
import io.jsonwebtoken.UnsupportedJwtException
import org.apache.commons.lang.StringUtils

import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import org.grails.web.util.WebUtils

class AuthorizationService {

    private Subject subject = null

    def grailsApplication

    public void handlerRequest(String[] scopes = []) {
        def request = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()
        if(!request){
            throw new UnauthorizedAccessError()
        }
        def realmId = request.getParameter("realmId")
        def jsonWebToken = request.getHeader("Authentication")
        if(!jsonWebToken){
            throw new InvalidTokenError()
        }
        if(!realmId || !realmId.equals(grailsApplication.config.app.realmId)){
            throw new UnauthorizedAccessError()
        }
        decodeJSONWebToken(realmId, scopes, jsonWebToken)
    }

    /**
     * get current subject
     * @return
     */
    public Subject getCurrentSubject(){
        return SubjectHolder.currentSubject
    }

    /**
     * set the current subject with given subject
     * @param subject
     */
    public void setCurrentSubject(Subject subject) {
        SubjectHolder.setCurrentSubject(subject)
    }

    /**
     * decode the given json web token and set the result to the subject
     * @param realmId
     * @param jsonWebToken
     */
    public void decodeJSONWebToken(String realmId, String[] scopes = [], String jsonWebToken) {
        jsonWebToken = jsonWebToken.trim()
        if (realmId != grailsApplication.config.app.realmId) {
            throw new UnauthorizedAccessError()
        }
        if (!jsonWebToken || StringUtils.isBlank(jsonWebToken)) {
            throw new MissingTokenError()
        }
        try {
            String realmPublicKey = grailsApplication.config.app.realm.publicKey
            PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(
                    new X509EncodedKeySpec(realmPublicKey.decodeBase64())
            );
            Jws<Claims> claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(jsonWebToken)
            if (realmId != claims.getBody().getAudience()) {
                throw new InvalidTokenError()
            }
            Subject subject = new Subject(
                    claims,
                    realmId
            )
            //check the scopes
            if (subject.getScopes().containsAll(scopes)) {
                throw new UnauthorizedAccessError()
            }
            setCurrentSubject(subject)
        } catch (FileNotFoundException ignored) {
            throw new FileNotFoundException("Public key file is not found!")
        }
        catch (ExpiredJwtException ignored) {
            throw new TokenExpiredError()
        } catch (UnsupportedJwtException ignored) {
            throw new InvalidTokenError()
        } catch (MalformedJwtException ignored) {
            throw new InvalidTokenError()
        } catch (SignatureException ignored) {
            throw new TokenTamperedError()
        } catch (IllegalArgumentException ignored) {
            throw new InvalidTokenError()
        }
    }
}

class UnauthorizedAccessError extends Exception {

}

class InvalidTokenError extends UnauthorizedAccessError {

}

class TokenExpiredError extends UnauthorizedAccessError {

}

class TokenTamperedError extends UnauthorizedAccessError {

}

class MissingTokenError extends UnauthorizedAccessError {

}

public class SubjectHolder {

    private static ThreadLocal<Subject> threadLocal = new ThreadLocal<Subject>()

    public static void setCurrentSubject(Subject subject) {
        threadLocal.set(subject)
    }

    public static Subject getCurrentSubject() {
        return threadLocal.get()
    }
}

public class Subject {

    private Jws<Claims> claims
    private String realmId

    private Subject(Jws<Claims> claims, String realmId) {
        this.claims = claims
        this.realmId = realmId
    }

    public String getAudience() {
        return this.claims.getBody().getAudience()
    }

    public String getSubject() {
        return this.claims.getBody().getSubject()
    }

    public String getClaims() {
        return this.claims.getBody().get('claims')
    }

    public String getRealmId() {
        return realmId
    }

    public List<String> getScopes() {
        return this.claims.getBody().get("scopes", []) as List
    }

    /**
     * render the subject as Map Object
     * @return
     */
    public Map<String, ?> toMap(){
        [
                audience: getAudience(),
                subject: getSubject(),
                claims: getClaims()
        ]
    }

}
