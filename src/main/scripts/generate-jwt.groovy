import groovyx.net.http.AsyncHTTPBuilder
import groovyx.net.http.ContentType

import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

description "Generate a test token", "grails generate-jwt"

// realmId : 099096a6-7dbe-46bb-846d-8676363835ed
// username : 3df41780-60fd-4cbc-8607-ba51a43aa7d2
// password : a92b6534-1175-4efd-8509-2c436a786de2
// test command: generate-jwt -s http://localhost:8080 -r 099096a6-7dbe-46bb-846d-8676363835ed -u 3df41780-60fd-4cbc-8607-ba51a43aa7d2 -p a92b6534-1175-4efd-8509-2c436a786de2

def REALM_SERVICE_URI = argsMap.s
def realmId = argsMap.r
def username = argsMap.u
def password = argsMap.p

def user_http = new AsyncHTTPBuilder(poolSize: 6, uri: REALM_SERVICE_URI)
Future user_result = user_http.post(
        path: "/api/v1/realms/" + realmId + "/tokens",
        query: [
                realmId : realmId,
                username: username,
                password: password,
                expiry  : 300000
        ],
        requestContentType: ContentType.JSON
)
def result = user_result.get(10, TimeUnit.SECONDS)

println "Generate test token: ${result.jwt}"