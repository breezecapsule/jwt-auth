import groovyx.net.http.AsyncHTTPBuilder
import groovyx.net.http.ContentType

import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

description "Create a realm from service with command sample: create-realm -s uri -r realmId -u username -p password", "grails create-realm"

// realmId : system
// username : superadmin
// password : 8f08e9cd-7f7c-41cf-b814-c9b283a3115e
// test command: create-realm -s http://localhost:8080 -r system -u superadmin -p 8f08e9cd-7f7c-41cf-b814-c9b283a3115e

def REALM_SERVICE_URI = argsMap.s
def realmId = argsMap.r
def username = argsMap.u
def password = argsMap.p

println "Send request to the useraa service to create a application realm ..."
println "Verifying ..."
def admin_http = new AsyncHTTPBuilder(poolSize: 6, uri: REALM_SERVICE_URI)
Future admin_result = admin_http.post(
        path: "/api/v1/realms/system/tokens",
        query: [
                realmId : realmId,
                username: username,
                password: password,
                expiry  : 300000
        ],
        requestContentType: ContentType.JSON
)
def super_admin_jwt = admin_result.get(10, TimeUnit.SECONDS).jwt
println "Creating application realm..."
def user_http = new AsyncHTTPBuilder(poolSize: 6, uri: REALM_SERVICE_URI)
user_http.setHeaders([Authorization: "Bearer " + super_admin_jwt])
Future user_result = user_http.post(
        path: "/api/v1/realms",
        query: [
                description: 'App with random Id: ' + UUID.randomUUID().toString()
        ],
        requestContentType: ContentType.JSON
)
def result = user_result.get(10, TimeUnit.SECONDS)

println "Application Realm Created! Please copy realm the id and public key to the config file:"
println "Application Realm Admin User: ${result.admin}"
println "Application Realm Id: ${result.id}"
println "Application Realm Public Key (base64 encoded): ${result.publicKey}"