/**
 * Created by FlyingAnt on 7/9/16.
 */


import annotation.JSONWebTokenRequest
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired
import user.auth.plugin.AuthorizationService;

@Aspect
public class UserAuthInterceptor {

    @Autowired
    def AuthorizationService authorizationService

    @Before("@annotation(jsonWebTokenRequest)")
    public void decodeJWTToken(JoinPoint jp, JSONWebTokenRequest jsonWebTokenRequest) {
        authorizationService.handlerRequest(jsonWebTokenRequest.value())
    }
}
